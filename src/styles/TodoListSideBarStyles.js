import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
    list: {
      width: 250,
    },
    fullList: {
      width: 'auto',
    },
    showButton: {
      color: '#34495e',
      fontSize: '1rem',
      fontWeight: '300',
    }
  });
  