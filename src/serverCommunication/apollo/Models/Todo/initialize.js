import queries from './queries';
import {CommunicationHelpers} from '../helpers';

const initialize = (client) => {
    let helpers = new CommunicationHelpers(client);
    return {
        getTodo: async (id) => (
            await helpers.fetchQuery(queries.getTodo, { id })
        ),
        getAllTodos: async () => (
            await helpers.fetchQuery(queries.getAllTodos)
        ),
        addTodo: async (todo) => {
            let answer = await helpers.fetchMutation(queries.addTodo, { todo });
            return answer;
        },
        editTodoTask: async (text, id) => (
            await helpers.fetchMutation(queries.editTodoTask, { text, id })
        ),
        deleteTodo: async (id) => (
            await helpers.fetchMutation(queries.deleteTodo, { id })
        ),
        updateFinishDate: async (finishDate, id) => (
            await helpers.fetchMutation(queries.updateFinishDate, { finishDate, id })
        ),
        removeFinishDate: async (id) => (
            await helpers.fetchMutation(queries.removeFinishDate, { id })
        ),
        toggleTodo: async (id) => (
            await helpers.fetchMutation(queries.toggleTodo, { id })
        )
    }
}

export default initialize;