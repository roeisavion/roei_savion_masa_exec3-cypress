import {
  REMOVE_TODO_LIST,
  SHOW_TODO_LIST,
  SET_STATE
} from '../actions/Types';
import initialState from '../constants/initialState';

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_TODO_LIST:
      if(action.listToShow.id === state.id){
        return state;
      }
      return action.listToShow;
    case REMOVE_TODO_LIST:
      if(action.listToRemove === state.id){
        return ''
      }
      break;
    case SET_STATE:
      return action.state.shownListName
    default:
      return state;
  }
};

export default reducer;
