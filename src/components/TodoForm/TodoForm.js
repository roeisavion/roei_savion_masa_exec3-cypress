import React from 'react';
import useInputState from '../../hooks/useInputState';
import useStyles from '../../styles/TodoFormStyles';

function TodoForm({addTodo, currentListName}) {
  const classes = useStyles();
  const [value, handleChange, clearValue] = useInputState('');
  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        if(currentListName.name === ''){
          alert('this is not a list');
        } else {
          addTodo(currentListName, value);
        }
        
        clearValue();
      }}
      className={classes.TodoForm}
      data-test='form'
    >
      <input
        placeholder="Add your task here..."
        value={value}
        onChange={handleChange}
        className={classes.input}
        data-test='input'
      />
    </form>
  );
}

export default TodoForm;
