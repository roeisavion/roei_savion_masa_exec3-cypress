import { connect } from 'react-redux'
import TodoList from './presentational/TodoList';

const mapStateToProps = state => ({
    todos: state.todos.filter((todo) => todo.shown),
    shownListName: state.shownListName
});

const mapDispatchToProps = (dispatch, {actions}) => ({
    removeTodo: id => dispatch(actions.removeTodo(id)),
    toggleTodo: (id) => dispatch(actions.toggleTodo(id)),
    editTodo: (task, id, listId) => dispatch(actions.editTodo(task, id, listId)),
    updateFinishDate: (finishDate, id) => dispatch(actions.updateFinishDate(finishDate, id)),
    removeFinishDate: (id) => dispatch(actions.removeFinishDate(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
