import React, { useCallback } from 'react';
import useInputState from '../../../hooks/useInputState';
import useStyles from '../../../styles/EditTodoFormStyles.js';

function EditTodoForm({ id, listId, task, toggleEditForm, editTodo }) {
  const classes = useStyles();
  const [value, handleChange, clearValue] = useInputState(task);

  const submit = useCallback(e => {
    e.preventDefault();
    editTodo(value, id, listId);
    toggleEditForm();
    clearValue();
  }, [value]);
  return (
    <form data-test='form'
      onSubmit={submit}
      className={classes.EditTodoForm}
    >
      <input data-test='input'
        autoFocus
        value={value}
        onChange={handleChange}
        onClick={e => e.stopPropagation()}
        className={classes.input}
      />
    </form>
  );
}

export default EditTodoForm;
