import React, { memo, useState } from 'react';
import EditTodoForm from './EditTodoForm';
import useToggleState from '../../../hooks/useToggleState';
import useStyles from '../../../styles/TodoStyles.js';
import FinishDate from './FinishDate';

function Todo({ id, listId, task, isCompleted, finishDate, toggleTodo, 
  removeTodo, editTodo, updateFinishDate, removeFinishDate}) {
  const classes = useStyles();
  const [isEditing, toggle] = useToggleState(false);
  const [completed, setCompleted] = useState(isCompleted);
  const toggleTask = () => {
    toggleTodo(id)
    setCompleted(!completed);
  }
  if (isEditing) {
    return (
      <li
        className={classes.Todo}
        style={{ overflowY: 'hidden' }}
        onClick={() => toggle()}
      >
        <EditTodoForm editTodo={editTodo} listId={listId} id={id} task={task} toggleEditForm={toggle} />
      </li>
    );
  }

  return (
    <div>
    <li
      className={classes.Todo}
      onClick={() => toggleTask()}
      data-test='TodoLine'
    >
      <span
        style={{
          textDecoration: completed ? 'line-through' : '',
          color: completed ? '#bdc3c7' : '#34495e'
        }}
      >
        {task}
      </span>
      <div className={classes.icons}>
        <i data-test='removeIcon'
          style={{ color: '#c0392b' }}
          className="fas fa-trash"
          onClick={e => {
            e.stopPropagation();
            removeTodo(id);
          }}
        />
        <i data-test='editIcon'
          style={{ color: '#58b2dc' }}
          className="fas fa-pen"
          onClick={e => {
            e.stopPropagation();
            toggle();
          }}
        />
      </div>
    </li>
    <FinishDate id={id} finishDate={finishDate} updateFinishDate={updateFinishDate} 
    removeFinishDate={removeFinishDate}/>
    </div>
  );
}

export default memo(Todo);
