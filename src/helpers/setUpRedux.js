import { createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk'
import rootReducer from '../reducers/root';
import {SET_STATE} from '../actions/Types';
import initialState from '../constants/initialState';

let store;
export default (communicator, isLocal) => {
  store = createStore(
      rootReducer,
      initialState,
      applyMiddleware(thunkMiddleware)
  )

  if(!isLocal) changeState(communicator);
  return store;
}

const changeState = async(communicator) => {
  let state;
  try{
    let todos = await communicator.getAllTodos();
    let lists = await communicator.getLists();
    if(lists.length === 0){
      await communicator.addList('default');
      lists = await communicator.getLists();
    }

    state = {
      listsNames: lists, 
      shownListName: {name: lists[0].name, id: lists[0].id},
      todos: todos.map(todo => todo.listId === lists[0].id ?
        {...todo, shown : true} : {...todo, shown : false}
      )
    };
  } catch(err) {
    console.error(err);
    state = initialState;
  }
  
  store.dispatch({type: SET_STATE, state: state})
}
